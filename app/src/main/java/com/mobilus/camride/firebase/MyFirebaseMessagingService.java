package com.mobilus.camride.firebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;
import com.mobilus.camride.R;
import com.mobilus.camtravelshared.activity.CamTaxiBookingDetailActivity;
import com.mobilus.camtravelshared.activity.CamTaxiChatActivity;
import com.mobilus.camtravelshared.activity.CamTaxiRentalDetailsActivity;
import com.mobilus.camtravelshared.activity.CamTaxiSightseeingDetailsActivity;
import com.mobilus.camtravelshared.activity.NotificationDialogActivity;
import com.mobilus.camtravelshared.config.Constants;
import com.mobilus.camtravelshared.model.Booking;
import com.mobilus.camtravelshared.model.Driver;
import com.mobilus.camtravelshared.model.OrderChat;
import com.mobilus.camtravelshared.retrofit.APIService;
import com.mobilus.camtravelshared.retrofit.APITypes;
import com.mobilus.utils.GsonUtils;
import com.mobilus.utils.LogUtils;
import com.mobilus.utils.NotificationUtils;
import com.mobilus.utils.retrofit.RetrofitCallback;

import java.util.Map;

/**
 * Created by Fikran on 02 Agu 2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    LocalBroadcastManager broadcastManager;
    private APIService apiService;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        LogUtils.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            LogUtils.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        if (remoteMessage.getNotification() != null) {
            LogUtils.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        apiService = APITypes.WITH_ACCESS_TOKEN.createService();
        Map<String, String> map = remoteMessage.getData();
        try{
            int notificationCode = Integer.parseInt(map.get("notification_code"));
            Log.i(TAG, "> Notification Code = " + notificationCode);

            if (notificationCode >= 700 && notificationCode < 800) { // CAMTAXI related
                handleOrderStatusUpdate(map);
            }else if(notificationCode == Constants.NOTIFICATION_CODE_NEW_CHAT_FROM_DRIVER){
                handleNewOrderChat(map);
            }else if(notificationCode == Constants.NOTIFICATION_CODE_CHAT_DELIVERED){
                handleOrderChatDelivered(map);
            }
        }catch (NumberFormatException e){
            e.printStackTrace();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    private void handleOrderStatusUpdate(Map<String, String> map){
        String sOrder = map.get("order");
        String sDriver = map.get("driver");
        Log.i(TAG, "> Booking = " + sOrder);
        Log.i(TAG, "> Driver = " + sDriver);

        Booking mOrder = Booking.fromJson(sOrder);
        Booking mOrderLocal = Booking.findByServerId(mOrder.getServerId());
        if(mOrderLocal != null){
            mOrder.setId(mOrderLocal.getId());
        }

        Driver mDriver = Driver.fromJson(sDriver);
        try {
            Driver mDriverLocal = Driver.findByServerId(mDriver.getServerId());
            if(mDriverLocal != null){
                mDriver.setId(mDriverLocal.getId());
            }
            mDriver.save();
            mOrder.setDriver(mDriver);
        }catch (NullPointerException e){}

        mOrder.setHighlighted(true);
        mOrder.save();

        //now, show the status bar notification
        Class<?> cls = CamTaxiBookingDetailActivity.class;
        if(mOrder.getServiceType() == Booking.SERVICE_TYPE_FULL_DAY_RENTAL
                || mOrder.getServiceType() == Booking.SERVICE_TYPE_HALF_DAY_RENTAL){
            cls = CamTaxiRentalDetailsActivity.class;
        }else if(mOrder.getServiceType() == Booking.SERVICE_TYPE_SIGHTSEEING_TOUR){
            cls = CamTaxiSightseeingDetailsActivity.class;
        }
        Intent notificationIntent = new Intent(getApplicationContext(), cls);
        notificationIntent.putExtra("order", Booking.getGson().toJson(mOrder));
        notificationIntent.putExtra("driver", sDriver);

        String title = getApplicationContext().getString(R.string.notif_title_booking_update);
        title = title.replaceFirst("#", "#" + mOrder.getServerId().toUpperCase());
        NotificationUtils notifBar = NotificationUtils.getInstance(
                getApplicationContext(),
                R.drawable.ic_notification_camride_small,
                R.drawable.ic_notification_camride
        );
        notifBar.setTag("booking_update");
        notifBar.setId(mOrder.getId().intValue());
        notifBar.notify(
                title,
                mOrder.getDisplayedStatus(getApplicationContext()),
                notificationIntent);

        Intent broadcastedData = new Intent(Constants.EVENT_ORDER_STATUS_UPDATE);
        broadcastedData.putExtra("order", Booking.getGson().toJson(mOrder));
        broadcastedData.putExtra("driver", sDriver);
        broadcastManager.sendBroadcast(broadcastedData);

        showDialog(title, mOrder.getDisplayedStatus(getApplicationContext()));

        Booking.save(getApplicationContext(), mOrder);
    }

    private void handleNewOrderChat(Map<String, String> map){
        Log.i(TAG, "> handleNewOrderChat");
        final String sChat = map.get("chat");
        Log.i(TAG, "> sChat = " + sChat);
        if(sChat == null){
            return;
        }
        OrderChat chat = OrderChat.fromServer(sChat);
        if(chat.getId() != null){
            return;
        }
        chat.save();

        apiService.markChatAsDelivered(chat.getServerId()).enqueue(new RetrofitCallback() {
            @Override
            public void onResponse(int status, JsonObject object) {
                LogUtils.i(TAG, "" + status);
                LogUtils.i(TAG, "" + object);
            }

            @Override
            protected String getTag() {
                return TAG;
            }
        });

        Intent data = new Intent(Constants.EVENT_NEW_CHAT);
        Log.i(TAG, "> order_chat = " + GsonUtils.getGson().toJson(chat));
        data.putExtra("order_chat", GsonUtils.getGson().toJson(chat));
        broadcastManager.sendBroadcast(data);

        String title = getApplicationContext().getString(R.string.notif_title_booking_chat);
        title = title.replaceFirst("#", "#" + chat.getBooking().getServerId().toUpperCase());
        String message = getString(R.string.new_message_from_driver);
        Intent notificationIntent = new Intent(getApplicationContext(), CamTaxiChatActivity.class);
        notificationIntent.putExtra("order", Booking.getGson().toJson(chat.getBooking()));
        Log.i(TAG, "> order = " + Booking.getGson().toJson(chat.getBooking()));

        NotificationUtils notificationUtils = NotificationUtils.getInstance(
                getApplicationContext(),
                R.drawable.ic_notification_camride_small,
                R.drawable.ic_notification_camride
        );
        notificationUtils.setTag("order_id");
        notificationUtils.setId((int)(long)chat.getBooking().getId());
        notificationUtils.notify(title, chat.getMessage(), notificationIntent);

        Bundle bundle = new Bundle();
        bundle.putString("order", Booking.getGson().toJson(chat.getBooking()));
        bundle.putInt("type", 1);
        showDialog(title, chat.getMessage(), bundle);
    }

    private void handleOrderChatDelivered(Map<String, String> map){
        String sId = map.get("id");
        if(sId == null){
            return;
        }
        OrderChat chat = OrderChat.findByServerId(sId);
        chat.setStatus(OrderChat.STATUS_DELIVERED);
        chat.save();

        Intent data = new Intent(Constants.EVENT_CHAT_DELIVERED);
        data.putExtra("id", chat.getId());
        broadcastManager.sendBroadcast(data);
    }

    private void showDialog(String title, String message){
        showDialog(title, message, null);
    }

    private void showDialog(String title, String message, Bundle data) {
//        Intent broadcastedData = new Intent(Constants.EVENT_NEW_NOTIFICATION);
//        broadcastManager.sendBroadcast(broadcastedData);

        Intent i = new Intent(getApplicationContext(), NotificationDialogActivity.class);
        i.putExtra("title", title);
        i.putExtra("message", message);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if(data != null){
            i.putExtras(data);
        }
        startActivity(i);
    }

    /*private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.ic_stat_ic_notification)
                .setContentTitle("FCM Message")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 *//* ID of notification *//*, notificationBuilder.build());
    }*/
}
