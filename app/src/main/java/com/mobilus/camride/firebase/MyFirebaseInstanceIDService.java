package com.mobilus.camride.firebase;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.JsonObject;
import com.mobilus.camtravelshared.config.Credential;
import com.mobilus.camtravelshared.model.Preference;
import com.mobilus.camtravelshared.retrofit.APITypes;
import com.mobilus.utils.LogUtils;
import com.mobilus.utils.retrofit.RetrofitCallback;

/**
 * Created by Fikran on 02 Agu 2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        LogUtils.d(TAG, "Refreshed token: " + refreshedToken);

        if(Credential.getUser() != null){
            sendRegistrationToServer();
        }
    }

    public static void sendRegistrationToServer() {
        boolean isTokenRegisteredOnServer = Preference.getAsBoolean(Preference.KEY__IS_FIREBASE_TOKEN_REGISTERED_ON_SERVER);
        if(isTokenRegisteredOnServer){
            return;
        }
        String firebaseToken = FirebaseInstanceId.getInstance().getToken();
        APITypes.WITH_ACCESS_TOKEN.createService().registerGcm(firebaseToken).enqueue(new RetrofitCallback() {
            @Override
            public void onResponse(int status, JsonObject object) {
                LogUtils.i(TAG, "status > " + status);
                LogUtils.i(TAG, "object > " + object);

                if(status > 0 && object != null){
                    LogUtils.i(TAG, "sendRegistrationToServer > success");
                    Preference.set(Preference.KEY__IS_FIREBASE_TOKEN_REGISTERED_ON_SERVER, true);
                }
            }
        });
    }

}
