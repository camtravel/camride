package com.mobilus.camride.config;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.mobilus.camride.BuildConfig;
import com.mobilus.camride.activity.HotspotDetailActivity;
import com.mobilus.camride.activity.HotspotImagePagerActivity;
import com.mobilus.camtravelshared.config.Baseconfig;
import com.mobilus.camtravelshared.enums.AppconfigKeys;
import com.mobilus.utils.LogUtils;
import com.orm.SugarContext;
import io.fabric.sdk.android.Fabric;

public class CamRideApp extends MultiDexApplication {

    private static final String TAG = CamRideApp.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        LogUtils.i(TAG, "onCreate");

        LogUtils.i(TAG, "map" + Baseconfig.map);

        SugarContext.init(this);

        LogUtils.i(TAG, "SugarContext.init");

        AppconfigKeys.APP_URL.saveValue("http://" + BuildConfig.SERVER_URL);
        AppconfigKeys.DEVELOPER_OPTION_ENABLED.saveValue("" + BuildConfig.ENABLE_DEVELOPER_OPTION);
        AppconfigKeys.GOOGLE_PROJECT_ID.saveValue(Appconfig.GOOGLE_PROJECT_ID);
        AppconfigKeys.GOOGLE_KEY.saveValue(Appconfig.GOOGLE_KEY);
        AppconfigKeys.GOOGLE_CLIENT_ID.saveValue(Appconfig.GOOGLE_CLIENT_ID);
        AppconfigKeys.FOURSQUARE_CLIENT_ID.saveValue(Appconfig.FOURSQUARE_CLIENT_ID);
        AppconfigKeys.FOURSQUARE_CLIENT_SECRET.saveValue(Appconfig.FOURSQUARE_CLIENT_SECRET);
        AppconfigKeys.PLACE_PROVIDER_AUTHORITY.saveValue(Appconfig.PLACE_PROVIDER_AUTHORITY);
        AppconfigKeys.PACKAGE_NAME.saveValue(Appconfig.PACKAGE_NAME);
        AppconfigKeys.HOTSPOT_DETAIL_ACTIVITY_CLASS_NAME.saveValue(HotspotDetailActivity.class.getName());
        AppconfigKeys.HOTSPOT_IMAGE_PAGER_ACTIVITY_CLASS_NAME.saveValue(HotspotImagePagerActivity.class.getName());

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        LogUtils.i(TAG, "AppconfigKeys");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
        LogUtils.i(TAG, "onTerminate");
    }

	@Override
	protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
		MultiDex.install(this);
        LogUtils.i(TAG, "attachBaseContext");
	}
}