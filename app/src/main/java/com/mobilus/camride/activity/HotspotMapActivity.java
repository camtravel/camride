package com.mobilus.camride.activity;

import android.os.Bundle;

import com.mobilus.camride.R;
import com.mobilus.camtravelshared.activity.BaseCamRideActivity;
import com.mobilus.camtravelshared.activityinitializer.HotspotMapActivityInitializer;

/**
 * Created by Fikran on 21 Jul 2016.
 */
public class HotspotMapActivity extends BaseCamRideActivity{
    private static final String TAG = HotspotMapActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        HotspotMapActivityInitializer hotspotMapActivityInitializer = new HotspotMapActivityInitializer(this);
        hotspotMapActivityInitializer.doInit();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_hotspot_map;
    }
}
