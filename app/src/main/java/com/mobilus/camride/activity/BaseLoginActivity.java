package com.mobilus.camride.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mobilus.camride.R;
import com.mobilus.camride.firebase.MyFirebaseInstanceIDService;
import com.mobilus.camtravelshared.config.Credential;
import com.mobilus.camtravelshared.model.Airport;
import com.mobilus.camtravelshared.model.Booking;
import com.mobilus.camtravelshared.model.Driver;
import com.mobilus.camtravelshared.model.RentalBookingSetting;
import com.mobilus.camtravelshared.model.SightseeingBookingSetting;
import com.mobilus.camtravelshared.model.User;
import com.mobilus.camtravelshared.retrofit.APIService;
import com.mobilus.camtravelshared.retrofit.APITypes;
import com.mobilus.utils.DeviceUtil;
import com.mobilus.utils.GsonUtils;
import com.mobilus.utils.LogUtils;
import com.mobilus.utils.retrofit.RetrofitCallback;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.rengwuxian.materialedittext.validation.RegexpValidator;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

import static com.mobilus.camtravelshared.config.Constants.EMAIL_PATTERN;

/**
 * Created by Fikran on 04 Agu 2016.
 */
public abstract class BaseLoginActivity extends AppCompatActivity {
    private static final String TAG = BaseLoginActivity.class.getSimpleName();

    private View vProgressBar;
    private View vContainerForm;
    private Button vSubmit;

    // Validators
    private METValidator emptyValidator;
    private RegexpValidator emailValidator;

    String deviceId;
    private APIService apiService;

    protected abstract int getContentView();
    protected abstract boolean isFormValid();
    protected abstract void onSubmit();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());

        initElements();
    }

    private void initElements(){
        apiService = APITypes.WITHOUT_ACCESS_TOKEN.createService();

        vProgressBar = findViewById(R.id.progress_bar);
        vContainerForm = findViewById(R.id.container_form);
        vSubmit = (Button)findViewById(R.id.submit);
        vSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });
    }

    protected METValidator getEmptyValidator() {
        if(emptyValidator == null){
            emptyValidator = new METValidator(getString(R.string.error_field_required)) {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    if(TextUtils.isEmpty(text)){
                        return false;
                    }
                    return true;
                }
            };
        }
        return emptyValidator;
    }

    protected RegexpValidator getEmailValidator() {
        if(emailValidator == null){
            emailValidator = new RegexpValidator(getString(R.string.error_invalid_email), EMAIL_PATTERN);
        }
        return emailValidator;
    }

    private void submit(){
        if(!isFormValid()){
            return;
        }

        String deviceId = getDeviceId();
        LogUtils.i(TAG, "deviceId > " + deviceId);
        if(TextUtils.isEmpty(deviceId)){
            showToastAnErrorOccurred();
            return;
        }

        showProgress(true);
        onSubmit();
    }

    public String getDeviceId() {
        if(TextUtils.isEmpty(deviceId)){
            deviceId = DeviceUtil.getDeviceId(getApplicationContext());
        }
        return deviceId;
    }

    protected void showProgress(boolean showProgress){
        if(showProgress){
            vContainerForm.setVisibility(View.GONE);
            vProgressBar.setVisibility(View.VISIBLE);
        }else{
            vProgressBar.setVisibility(View.GONE);
            vContainerForm.setVisibility(View.VISIBLE);
        }
    }

    protected void goToInitActivity(){
        Intent intent = new Intent(getApplicationContext(), InitActivity.class);
        startActivity(intent);

        finish();
    }

    protected APIService getApiService() {
        return apiService;
    }

    protected void showToastAnErrorOccurred(){
        Toast.makeText(
                getApplicationContext(),
                R.string.an_error_occurred,
                Toast.LENGTH_LONG
        ).show();
    }

    protected RetrofitCallback getSubmitCallback(){
        return new RetrofitCallback() {
            @Override
            public void onResponse(int status, JsonObject object) {
                LogUtils.i(TAG, "status > " + status);
                LogUtils.i(TAG, "object > " + object);
                if(status < 1 || object == null){
                    showProgress(false);
                    if(object == null){
                        showToastAnErrorOccurred();
                    }else if(object.has("msg")){
                        String msg = object.get("msg").getAsString();
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                    return;
                }
                LogUtils.i(TAG, "login > success");
                JsonObject oData = object.getAsJsonObject("data");
                JsonObject oUser = oData.getAsJsonObject("user");
                String accessToken = oData.get("auth_token").getAsString();

                User user = new User(oUser);
                user.setAccessToken(accessToken);
                user.save();

                Credential.setUser(user);

                MyFirebaseInstanceIDService.sendRegistrationToServer();

//                goToInitActivity();
                initDataAfterLogin();
            }

            @Override
            protected String getTag() {
                return TAG;
            }
        };
    }

    private void initDataAfterLogin(){
        apiService.getCamrideInitialData().enqueue(new RetrofitCallback() {
            @Override
            public void onResponse(int status, JsonObject object) {
                if(status < 1 && object == null){
                    return;
                }

                JsonObject oData = object.getAsJsonObject("data");

                // Airport
                Type listType = new TypeToken<List<Airport>>() {}.getType();
                List<Airport> airports = GsonUtils
                        .getGson()
                        .fromJson(oData.getAsJsonArray("airports"), listType);
                Airport.saveInTx(airports);

                // SightseeingBookingSetting
                listType = new TypeToken<List<SightseeingBookingSetting>>() {}.getType();
                List<SightseeingBookingSetting> sightseeingBookingSettings = GsonUtils
                        .getGson()
                        .fromJson(oData.getAsJsonArray("sightseeing_booking_settings"), listType);
                SightseeingBookingSetting.saveInTx(sightseeingBookingSettings);

                // RentalBookingSetting
                listType = new TypeToken<List<RentalBookingSetting>>() {}.getType();
                List<RentalBookingSetting> rentalBookingSettings = GsonUtils
                        .getGson()
                        .fromJson(oData.getAsJsonArray("rental_booking_settings"), listType);
                RentalBookingSetting.saveInTx(rentalBookingSettings);

                // Driver
                listType = new TypeToken<List<Driver>>() {}.getType();
                List<Driver> drivers = GsonUtils
                        .getGson()
                        .fromJson(oData.getAsJsonArray("drivers"), listType);
                Driver.saveInTx(drivers);

                HashMap<String, Driver> driverMap = new HashMap<>();
                for(Driver driver : drivers){
                    driverMap.put(driver.getServerId(), driver);
                }

                // Booking
                JsonArray oBookings = oData.getAsJsonArray("bookings");
                for(int i = 0; i < oBookings.size(); i++){
                    JsonObject oBooking = oBookings.get(i).getAsJsonObject();
                    Booking booking = GsonUtils.getGson().fromJson(oBooking, Booking.class);

                    String driverIdServer = oBooking.get("driver_id").getAsString();
                    Driver driver = driverMap.get(driverIdServer);

                    booking.setServerDriverId(driverIdServer);
                    booking.setDriver(driver);

                    booking.save();
                }

                goToInitActivity();
            }
        });
    }
}
