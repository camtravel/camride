package com.mobilus.camride.activity;

import android.os.Bundle;

import com.mobilus.camride.R;
import com.mobilus.camtravelshared.activity.BaseCamRideActivity;

public class HotspotImagePagerActivity extends BaseCamRideActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_hotspot_image_pager;
    }
}
