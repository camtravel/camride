package com.mobilus.camride.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.gson.JsonObject;
import com.mobilus.camride.R;
import com.mobilus.camtravelshared.enums.AppconfigKeys;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

/**
 * Created by Fikran on 04 Agu 2016.
 */
public class RegisterActivity extends BaseLoginActivity {
    private static final String TAG = RegisterActivity.class.getSimpleName();

    private MaterialEditText vName;
    private MaterialEditText vPhone;
    private MaterialEditText vEmail;
    private MaterialEditText vPassword;
    private MaterialEditText vRePassword;
    private View vContainerSignInButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initElements();
    }

    private void initElements(){
        vName = (MaterialEditText)findViewById(R.id.name);
        vPhone = (MaterialEditText)findViewById(R.id.phone);
        vEmail = (MaterialEditText)findViewById(R.id.email);
        vPassword = (MaterialEditText)findViewById(R.id.password);
        vRePassword = (MaterialEditText)findViewById(R.id.repassword);
        vContainerSignInButton = findViewById(R.id.container_sign_in_button);

        vName.addValidator(getEmptyValidator());
        vEmail.addValidator(getEmptyValidator())
                .addValidator(getEmailValidator());
        vPassword.addValidator(getEmptyValidator())
                .addValidator(new METValidator(getString(R.string.error_invalid_password)) {
                    @Override
                    public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                        if(text.length() < 6){
                            return false;
                        }

                        return true;
                    }
                });
        vRePassword.addValidator(new METValidator(getString(R.string.password_does_not_match_the_confirm_password)) {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                if(!text.toString().equals(vPassword.getText().toString())){
                    return false;
                }

                return true;
            }
        });

        vContainerSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToLoginActivity();
            }
        });
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_register;
    }

    @Override
    protected boolean isFormValid() {
        return vName.validate()
                && vEmail.validate()
                && vPassword.validate()
                && vRePassword.validate();
    }

    @Override
    protected void onSubmit() {
        JsonObject oUser = new JsonObject();
        oUser.addProperty("name", vName.getText().toString());
        oUser.addProperty("phone", vPhone.getText().toString());
        oUser.addProperty("username", vEmail.getText().toString());
        oUser.addProperty("password", vPassword.getText().toString());
        oUser.addProperty("deviceId", getDeviceId());
        oUser.addProperty("android_app_id", AppconfigKeys.PACKAGE_NAME.getValue());

        getApiService().signUp(oUser).enqueue(getSubmitCallback());
    }

    private void goToLoginActivity(){
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);

        finish();
    }
}
