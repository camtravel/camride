package com.mobilus.camride.activity;

import com.mobilus.camtravelshared.activity.BaseHotspotDetailActivity;
import com.mobilus.camtravelshared.adapter.HotspotImageAdapter;

public class HotspotDetailActivity extends BaseHotspotDetailActivity{

    @Override
    protected HotspotImageAdapter getHotspotImageAdapter() {
        return new HotspotImageAdapter(this,
                getHotspotImageProgressBar(), getImages(), getHotspot());
    }

    @Override
    protected Class<?> getHotspotMapActivityClass() {
        return HotspotMapActivity.class;
    }

    @Override
    protected Class<?> getDealDetailActivityClass() {
        return DealDetailActivity.class;
    }

}
