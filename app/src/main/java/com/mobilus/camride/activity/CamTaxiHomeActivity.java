package com.mobilus.camride.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.mobilus.camtravelshared.R;
import com.mobilus.camtravelshared.activity.BaseCamRideActivity;
import com.mobilus.camtravelshared.config.Baseconfig;
import com.mobilus.camtravelshared.config.Credential;
import com.mobilus.camtravelshared.model.AppconfigDb;

import java.util.List;

import static com.mobilus.camtravelshared.config.Constants.PREF_SYNC_STATES;

/**
 * Created by Fikran on 05 Jun 2015.
 */
public class CamTaxiHomeActivity extends BaseCamRideActivity {
    ProgressDialog progressDialog;

    private static final String TAG = CamTaxiHomeActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.title_activity_camtaxi);
        setNavigationLogoDrawable(null);
    }

    @Override
    protected void onClickNavigationLogo(View view) {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.acitivity_cam_taxi_home;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_camride_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            new LogoutTask().execute();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class LogoutTask extends AsyncTask<String, Void, Void> {
        public LogoutTask() {
            CamTaxiHomeActivity.this.progressDialog = new ProgressDialog(CamTaxiHomeActivity.this);
            CamTaxiHomeActivity.this.progressDialog.setMessage(CamTaxiHomeActivity.this.getString(R.string.logging_out));
        }

        @Override
        protected Void doInBackground(String... params) {
            /** Remove all shared preferences **/

            /*
            CamTaxiHomeActivity.this.getSharedPreferences(
                    getString(R.string.package_name), Context.MODE_PRIVATE).edit().clear().commit();
            */

            Credential.clear();

            CamTaxiHomeActivity.this.getSharedPreferences(
                    PREF_SYNC_STATES, Context.MODE_PRIVATE).edit().clear().commit();

            List<AppconfigDb> appconfigDbList = AppconfigDb.listAll(AppconfigDb.class);

            /** Reset Database **/
            Baseconfig.resetDB(getApplicationContext());

            AppconfigDb.saveInTx(appconfigDbList);
            /** Unregister GCM from server **/
            /*
            String regId = GCMRegistrar.getRegistrationId(CamTaxiHomeActivity.this);
            if(!regId.isEmpty()){
                GCMUtil.unregister(CamTaxiHomeActivity.this, regId);
            }
            */

            FacebookSdk.sdkInitialize(getApplicationContext());
            LoginManager.getInstance().logOut();

            return null;
        }

        protected void onPreExecute() {
            CamTaxiHomeActivity.this.progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void mVoid) {
            try {
                if ((CamTaxiHomeActivity.this.progressDialog != null)
                        && CamTaxiHomeActivity.this.progressDialog.isShowing()) {
                    CamTaxiHomeActivity.this.progressDialog.dismiss();
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                CamTaxiHomeActivity.this.progressDialog = null;
            }

            finishLogout();
        }
    }

    private void finishLogout() {
        Intent intent = new Intent(getApplicationContext(), InitActivity.class);
        startActivity(intent);
        finish();
    }
}
