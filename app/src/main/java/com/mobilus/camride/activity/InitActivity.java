package com.mobilus.camride.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.mobilus.camtravelshared.config.Credential;

/**
 * Created by Fikran on 04 Agu 2016.
 */
public class InitActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_LOGIN_ACTIVITY = 1;
    private static final int REQUEST_CODE_CAMRIDE_HOME_ACTIVITY = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(Credential.getUser() == null){
            goToLoginActivity();
        }else{
            goToCamTaxiHomeActivity();
        }
    }

    private void goToLoginActivity(){
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivityForResult(intent, REQUEST_CODE_LOGIN_ACTIVITY);

        finish();
    }

    private void goToCamTaxiHomeActivity(){
        Intent intent = new Intent(getApplicationContext(), CamTaxiHomeActivity.class);
        startActivityForResult(intent, REQUEST_CODE_CAMRIDE_HOME_ACTIVITY);

        finish();
    }
}
