package com.mobilus.camride.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mobilus.camride.R;
import com.mobilus.camtravelshared.enums.AppconfigKeys;
import com.mobilus.utils.InputMethodManagerUtils;
import com.mobilus.utils.LogUtils;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.Arrays;

/**
 * Created by Fikran on 03 Agu 2016.
 */
public class LoginActivity extends BaseLoginActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 1;

    private MaterialEditText vEmail;
    private MaterialEditText vPassword;
    private View vContainerSignUpButton;
    private ImageView vGoogleBtn;
    private ImageView vFbBtn;
    private View vOverlay;

    // Facebook
    private CallbackManager fbCallbackManager;

    // Google
    GoogleApiClient mGoogleApiClient;

    private String email = "";
    private String password = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initElements();
    }

    private void initElements(){
        vEmail = (MaterialEditText)findViewById(R.id.email);
        vPassword = (MaterialEditText)findViewById(R.id.password);
        vContainerSignUpButton = findViewById(R.id.container_sign_up_button);
        vGoogleBtn = (ImageView)findViewById(R.id.google_btn);
        vFbBtn = (ImageView)findViewById(R.id.fb_btn);
        vOverlay = findViewById(R.id.overlay);

        vEmail.addValidator(getEmptyValidator())
                .addValidator(getEmailValidator());
        vPassword.addValidator(getEmptyValidator());

        vContainerSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToRegisterActivity();
            }
        });

        // Init Facebook
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        fbCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(fbCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                LogUtils.i(TAG, "fb login - onSuccess");
                showOverlay(false);
                LogUtils.i(TAG, "fb login - loginResult > " + loginResult.toString());

                String token = loginResult.getAccessToken().getToken();
                LogUtils.i(TAG, "loginResult.getAccessToken" + token);
                for(String permission : loginResult.getRecentlyGrantedPermissions()){
                    LogUtils.i(TAG, "RecentlyGrantedPermission : " + permission);
                }
                for(String permission : loginResult.getRecentlyDeniedPermissions()){
                    LogUtils.i(TAG, "RecentlyDeniedPermission : " + permission);
                }

                loginWithFb(token);
            }

            @Override
            public void onCancel() {
                LogUtils.i(TAG, "fb login - onCancel");
                showOverlay(false);
            }

            @Override
            public void onError(FacebookException error) {
                LogUtils.i(TAG, "fb login - error > " + error.getMessage());
                showOverlay(false);
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        vFbBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogUtils.i(TAG, "onClick FbBtn");
                showOverlay(true);
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email"));
            }
        });

        //Init Google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(AppconfigKeys.GOOGLE_CLIENT_ID.getValue())
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        vGoogleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogUtils.i(TAG, "onClick GoogleBtn");
                showOverlay(true);
                loginWithGoogle();
            }
        });
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isFormValid(){
        return vEmail.validate() && vPassword.validate();
    }

    @Override
    protected void onSubmit() {
        email = vEmail.getText().toString();
        password = vPassword.getText().toString();

        getApiService()
                .signIn(
                        email,
                        password,
                        deviceId,
                        AppconfigKeys.PACKAGE_NAME.getValue()
                ).enqueue(getSubmitCallback());
    }

    private void goToRegisterActivity(){
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(intent);

        finish();
    }

    private void showOverlay(boolean show){
        InputMethodManagerUtils.hideSoftKeyboard(this);

        if(show){
            vOverlay.setVisibility(View.VISIBLE);
        }else{
            vOverlay.setVisibility(View.GONE);
        }
    }

    private void loginWithFb(String fbToken){
        String deviceId = getDeviceId();
        if(TextUtils.isEmpty(deviceId)){
            showToastAnErrorOccurred();
            return;
        }
        showProgress(true);
        getApiService().signInWithFacebook(
                    fbToken,
                    deviceId,
                    AppconfigKeys.PACKAGE_NAME.getValue())
                .enqueue(getSubmitCallback());
    }

    private void loginWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        LogUtils.d(TAG, "requestCode:" + requestCode);
        LogUtils.d(TAG, "resultCode:" + resultCode);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSignInResult(result);
            return;
        }else if(fbCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }

    private void handleGoogleSignInResult(GoogleSignInResult result) {
        LogUtils.d(TAG, "handleSignInResult:" + result.isSuccess());
        showOverlay(false);
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String idToken = acct.getIdToken();
            String msg = "Signed in success\n";
            msg += "DisplayName: " + acct.getDisplayName() + "\n";
            msg += "Id: " + acct.getId() + "\n";
            msg += "Email: " + acct.getEmail() + "\n";
            msg += "IdToken: " + idToken + "\n";
            msg += "ServerAuthCode: " + acct.getServerAuthCode();

            LogUtils.i(TAG, "handleSignInGoogleResult > \n" + msg);

            String deviceId = getDeviceId();
            if(TextUtils.isEmpty(deviceId)){
                showToastAnErrorOccurred();
                return;
            }

            showProgress(true);
            getApiService().signInWithGoogle(
                    idToken,
                    deviceId,
                    AppconfigKeys.PACKAGE_NAME.getValue()
            ).enqueue(getSubmitCallback());
        }else{
            showToastAnErrorOccurred();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
